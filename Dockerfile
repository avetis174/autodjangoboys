FROM python:3-alpine

ARG user=admin
ARG email=admin@example.com
ARG password=pass

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt && \
     mkdir db

COPY . /app

EXPOSE 8000

VOLUME ["/app/db"]

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
